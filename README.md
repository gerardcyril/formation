# Formation POEI

## Voici les fichiers créés durant la formation POEI chez Capensis

- [conf](conf)
- [script](script-utils)
- [python](python)
# Paquets sympas

- Terminal multi-screen : terminator
- Calcul de réseau IP : ipcalc
- Supervision basique : glances
- Paquet .deb .rpm : alien 
- **Attention**, *:(){ :|:& };:*

# Liens Utiles

- Prompt du .bashrc : http://bashrcgenerator.com

# LIENS :

- Astuce en shell : https://www.jujens.eu/posts/2014/Sep/07/diverses-astuces-shell/
- 100 shell script examples : https://github.com/epety/100-shell-script-examples
- https://github.com/awesome-lists/awesome-bash
- Awesome Bash : https://news.ycombinator.com/item?id=14769384
- Bash shell scripting : https://natelandau.com/bash-scripting-utilities/
- LAMP Centos7 : https://www.howtoforge.com/tutorial/centos-lamp-server-apache-mysql-php/
- SSL with HAProxy : https://www.supinfo.com/articles/single/2192-terminaison-ssl-load-balancing-avec-haproxy
- Ansible : https://wiki.evolix.org/HowtoAnsible

