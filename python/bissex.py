#!/usr/bin/env python
import math

saisie = input("Quel est l'année :")
annee = int(saisie)

if annee % 400 == 0 or ( annee % 4 == 0 and annee % 100 != 0):
	print("L'année", annee, "est bissextile")
else:
	print("L'année", annee, "n'est pas bissextile")