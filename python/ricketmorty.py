#!/usr/bin/env python

import requests

# Les variables utilisees
URL = "http://192.168.20.245:8080/api/"
liste_lieux = ["Space station", "Microverse", "TV", "Dream"]
liste_carac = []
final_list = []
csv_file = ","

# Parcours des lieux qui m'interesse 
for location in liste_lieux:
    # recuperation et mise au format json des lieux
    content = requests.get(URL + "location/?type=" + location).json()
    # Parcours du details des lieux
    for lieux in content["results"]:
        # Ajout du numero du personnage dans une liste
        liste_carac = liste_carac + [x.split("/")[5] for x in lieux.get('residents')]
# On retire les doublons dans la liste
liste_carac = list(set(liste_carac))

# Parcours de la liste des personnages
for i in liste_carac:
    # Recupération et mise au format json de la page du personnage
    content_char = requests.get(URL + "character/" + i).json()
    # Verification sur le status et l'espece de mon personnage
    if content_char.get("status") != "unknown" and content_char.get("species") != "Animal":
        # Recuperation et mise au format json de la page du premier episode dans lequel mon personnage apparait
        content_epi = requests.get(URL + "episode/" + content_char.get("episode")[0].split("/")[5]).json()
        # Creation de mon dictionnaire de personnage et recuperation des donnees
        monperso = {
            "ID":content_char.get("id"),
            "Nom":content_char.get("name"),
            "Espece":content_char.get("species"),
            "Type":content_char.get("type"),
            "Lieu":content_char.get("location").get("name"),
            "Episode":content_epi.get("episode"),
            "NomEpisode":content_epi.get("name"),
            "DateApparition":content_epi.get("created")[0:10]
        }
        # Ajout de mon dictionnaire a ma liste finale
        final_list.append(monperso)
# Parcours des dictionnaires de ma liste
for dico in final_list:
    # Ajout dans le fichier un passage a la ligne
    csv_file = "{} \n".format(csv_file[:-1])
    # Parcours des champs de mes personnages
    for champs in dico.values():
        # Ajout des champs de mes personnages au fichier 
        csv_file = "{}{},".format(csv_file, champs)
# Ecriture dans "fichier.csv" 
with open("fichier.csv", 'w+') as file:
    file.write(csv_file)
file.close()
