#!/usr/bin/env python

import re
import sys

saisie = input("Dit moi tout\n")
toto=True

# Verification que les mots commencent par des majuscules
for mot in saisie.split(" "):
	if not re.search("[A-Z0-9]\w*", mot):
		toto=False
		print("La phrase n'est pas bonne")
		sys.exit(1)
if toto:
	print ("La phrase est bonne !")

# Capture des groupes
match = re.findall(r"([0-9]+\s\w+)", saisie)
print(match)
