#!/usr/bin/env python

from math import sqrt
from argparse import ArgumentParser
import time

parser = ArgumentParser()
parser.add_argument("--integer")
parser.add_argument("--output")
args = parser.parse_args()

ajrd = time.strftime("%d %b %Y %H-%M-%S")
racineclubdelens = sqrt(int(args.integer))

try:
	with open(args.output, 'a+') as file:
		file.write(" {} \t {}\n".format(ajrd, racineclubdelens))
except: 
	print(ajrd, racineclubdelens)