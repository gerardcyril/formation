#!/usr/bin/env python
import json
import sys

def attention (ligne):
	try :
		return int(ligne.split(":")[2])
	except:
		print("t'es con")
		sys.exit(1)

# Ouverture du fichier
with open('/etc/passwd', 'r') as file:
    contenu = file.readlines()

# Extraction des donnees
liste = []
for ligne in contenu:
	mondico = {
		"user": ligne.split(":")[0], 
		"id": attention(ligne), 
		"home": ligne.split(":")[5]
	}
	liste.append(mondico)

# Mise au format Json et export
with open('result.json', 'w+') as file:
	json.dump(liste, file, indent=4)
