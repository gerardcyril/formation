#!/usr/bin/env python
import re

fd = open('monpremierprogramme.py', 'r')
t = len(fd.readlines())
print(t)

with open('monpremierprogramme.py', 'r') as file:
    contenu = file.read()
    # Creation d'une expression contenant tout les caracteres non-alpha-numerique
    mon_expr = re.compile("\W+", re.U)
    liste = mon_expr.split(contenu)
    for mot in liste:
        if len(mot) < 1:
            liste.remove(mot)
    print(len(liste))
file.close()
