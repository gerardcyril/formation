#!/bin/bash

yum install bind bind-utils

cp ../conf/named.conf /etc/named.conf

cp ../conf/cyril.pizza.zone /var/named/
cp ../conf/cyril.pizza.reverse /var/named/
