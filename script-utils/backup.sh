#!/bin/bash

ajrd=$(date +"%m-%d-%y")
green=$(tput setaf 76)
reset=$(tput sgr0)
e_success() { printf "${green}✔ %s${reset}\n" "$@" 
}

while true 
do

read -p "(S)auvegarder ou (R)estaurer ? " reponse

case $reponse in
   S|s)
	read -p "Ou veux-tu sauvegarder tes donnees ?  " destination
	
        # Création des dossiers de sauvegarde
		savedata=$destination/save-$ajrd
		mkdir -p $destination/save-$ajrd
		mkdir -p $savedata/install 
		
	# Sauvegarde du dossier /etc/
		tar -cvzf $savedata/etc-$ajrd.tar.gz "/etc/"
		e_success "Sauvegarde de /etc/ réussie"
	# Sauvegarde des repertoire /home/
		for rep in /home/*
		do
			mkdir -p $savedata$rep && cp -ru * $savedata$rep
			e_success "Sauvegarde de $rep réussie"
		done
	
	# Sauvegarde de la liste des paquets et des serveurs
		rpm -qa > $savedata/install/paquets
		cp -u /etc/yum.repos.d/* $savedata/install/
		break ;;

   R|r)
	echo blabla restauration
	break ;;

   *)
	echo "Apprends a lire"
esac
done
