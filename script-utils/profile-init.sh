#!/bin/bash

# Permet de configurer une machine centos.
#
# usage: profile-init [OPTIONS]
#
# OPTIONS
#
#     -h,  --help          Output a usage message
#     -b,  --bash          Change bashrc
#     -n,  --vivaldi       Install Vivaldi
#     -p,  --paquets       Install basic packages
#     -s,  --sublime       Install Sublime Text
#     -v,  --virtualbox    Install VirtualBox
#

source /home/$USER/lib/color.sh

sublime(){
	rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
	yum-config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
	yum -y install sublime-text
}

install_packages(){
	mes_paquets="curl vim tree git epel-release sudo cowsay"
	for paquets in $mes_paquets ; do
		yum -y install $paquets > /dev/null && e_success "$paquets installed" || e_error "$paquets failed"
	done
}

virtualbox() {
	wget -P  /etc/yum.repos.d/ http://download.virtualbox.org/virtualbox/rpm/rhel/virtualbox.repo
	yum install -y gcc make kernel-devel kernel-headers dkms kernel-devel kernel-devel-3.10.0-862.3.3.el7.x86_64
	yum install -y VirtualBox-5.2
	usermod -a -G vboxusers $USER && e_success "VirtualBox Installed" || e_error "Installation failed"
}

vivaldi() {
	yum install -y https://downloads.vivaldi.com/stable/vivaldi-stable-2.0.1309.29-2.x86_64.rpm
	yum install -y http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm
	yum install -y flash-player-ppapi
}

bashrc() { 
	curl https://gitlab.com/gerardcyril/formation/raw/master/conf/bashrc > ~/.bashrc
}

doc(){ sed -n '2,/^$/ { s/^ *#// ; s/^ //g ; t ok ; d ; :ok ; p }' <$0 ; }

while [ ! -z "$1" ] ;
do
    case "$1" in
    	"-h"|"--help") doc && exit ;;
        "-b"|"--bash") bashrc && e_success "bash configured" && source ~/.bashrc ;;
		"-n"|"--vivaldi") vivaldi && e_success "Vivaldi Installed" || e_error "Installation failed" ;;
		"-p"|"--paquets") install_packages ;;
		"-s"|"--sublime") sublime && e_success "Sublime Text Installed" || e_error "Installation failed" ;;
		"-v"|"--virtualbox") virtualbox ;;
        *) doc && break;;
    esac
    shift
done
