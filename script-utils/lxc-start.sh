#!/bin/bash -eu

if test -z "$*"
then
	CONTAINERS=$(ls /var/lib/lxc/)
else
  if [[ $1 == '-h' ]];
  then
    echo "$(basename "$0") [-h] [arguments] -- programme pour lancer des containers LXC

    Exemple:
        $(basename "$0") -h             Montre l'aide
        $(basename "$0")          Lance tous les containers
        $(basename "$0") [arguments]  Lance le(s) container(s) que vous avez passé en argument"
    exit 0;
  fi
	CONTAINERS=$*
fi
for container in $CONTAINERS; do
  if [ "$(lxc-info -n $container -s)" != "State:          STOPPED" ]; then
    echo "'$container' is already running, skipping."
  else
    echo "Starting '$container'..."
    lxc-start -dn $container &
  fi
done
