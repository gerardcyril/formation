#!/bin/bash

# Pour une machine centos, il faudra décommenter la ligne 17 et 19

while [ -z "$CONTAINER_NAME" ]
do
  read -p "Donnez le nom du nouveau conteneur: " CONTAINER_NAME
done

sudo lxc-create --template download --name "$CONTAINER_NAME" -- -d debian -r stretch -a amd64
sudo lxc-start -n "$CONTAINER_NAME" -d
sleep 5
sudo lxc-attach -n "$CONTAINER_NAME" -- apt update
sleep 5
sudo lxc-attach -n "$CONTAINER_NAME" -- apt -y upgrade
sudo lxc-attach -n "$CONTAINER_NAME" -- apt -y install openssh-server python sudo
# sudo lxc-attach -n "$CONTAINER_NAME" -- systemctl start sshd
sudo lxc-attach -n "$CONTAINER_NAME" -- adduser $USER
# sudo lxc-attach -n "$CONTAINER_NAME" -- passwd $USER
sudo lxc-attach -n "$CONTAINER_NAME" -- usermod -a -G sudo $USER
