<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wordpress');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'user');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'user');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Fm[[exd_iI_<xl0pA6HFU. m1fA{%^s7 #Q#1^Y+0rU^Es5f@K-U~f;>=reu|s;Z');
define('SECURE_AUTH_KEY',  'k^Xmpp:I~BtB>QMh2O=@v;94KT2!@`lr6it}P1R$p2p^QAvY&]/|/d[R!*?]SH+=');
define('LOGGED_IN_KEY',    'v+QI#4-w^[2|Zb=yKSggq!`;-]m_)h}&)Hv%bzsx&]{_m:]Lz=&=*vOlPPEbBV }');
define('NONCE_KEY',        'Jj{5}qeAU?ECsgs$w5Y{V:7QVeCw,eC*)W;RWv2?7qe*sj2TngG4i d^ucOo$k2)');
define('AUTH_SALT',        'K__LQ>,.CmUL+eWL&5BIHre?38Z4Knf-;a*k?UV|xM}.$o+nP }#:uop6kc!zoOz');
define('SECURE_AUTH_SALT', 'Z&z1bV0+y(8}p 8>(?krD.O6sQ^II]_M{7^Pg|aRG$0j~Yi* %3uJolm0I@wPeE4');
define('LOGGED_IN_SALT',   'O(0^HsF^4V1Q?8r~ kmuE|Erk1c2Lh$>QxN12&LyYcW~%1lhRA+n*d,?`dYs[q9!');
define('NONCE_SALT',       '6p7kL_wfX~:nPDxCxJJjYhoPwCIcGrU1T8:et?`08Eb.67|oK9hIXQVS+~5#:#m^');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
