# ~/.bashrc: executed by bash(1) for non-login shells.

# Masque de creation rwxr-xr-x
umask 022

# On fixe le PATH
export PATH=${HOME}/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/bin/X11:${PATH}

# On evite les betises
alias cp='cp -i'
alias rm='rm -i'
alias mv='mv -i'

# Quelques raccourcis
alias la='ls -al --color'
alias ll='ls -l --color'
alias cls='clear ; ls --color'
alias cla='clear ; ls -al --color'
alias cll='clear ; ls -l --color'
alias lsize='ls --sort=size -lhr'
alias tree='tree -Cs'
alias grep='grep --color'
alias please='sudo $(history -p \!\!)'
alias reload='source ~/.bashrc'

alias ports='netstat -laputn'
alias vi='vim'

# Un petit truc pour les copies de sauvegarde de fichiers
old ()
{
    if test "x$1" = "x" ; then
        echo "old: erreur de syntaxe."
	exit 1
    fi

    local qui
    read -e -p "qui ? " qui

    for i in $*
    do
        new_name="$1.old-`date '+%Y%m%d-%H%M%S-%N'`"-${qui}
	
	if test -L "$1" -o -d "$1" ; then
	    mv $1 ${new_name}
	else
	    cp $1 ${new_name}
	fi
	shift
    done
}
export old

# Easy extract
extract () {
  if [ -f $1 ] ; then
      case $1 in
          *.tar.bz2)   tar xvjf $1    ;;
          *.tar.gz)    tar xvzf $1    ;;
          *.bz2)       bunzip2 $1     ;;
          *.rar)       rar x $1       ;;
          *.gz)        gunzip $1      ;;
          *.tar)       tar xvf $1     ;;
          *.tbz2)      tar xvjf $1    ;;
          *.tgz)       tar xvzf $1    ;;
          *.zip)       unzip $1       ;;
          *.Z)         uncompress $1  ;;
          *.7z)        7z x $1        ;;
          *)           echo "don't know how to extract '$1'..." ;;
      esac
  else
      echo "'$1' is not a valid file!"
  fi
}

# On charge d'autres alias et functions si necessaire
test -r $HOME/.alias && source $HOME/.alias

# En mode non interactif on ne fait rien de plus
test -z "$PS1" && return

# On fixe le prompt
export PS1="\[\033[38;5;1m\]\u\[$(tput sgr0)\]\[\033[38;5;2m\]@\h\[$(tput sgr0)\]\[\033[38;5;15m\] : \[$(tput sgr0)\]\[\033[38;5;11m\]\w\[$(tput sgr0)\]\[\033[38;5;15m\]\n\\$ \[$(tput sgr0)\]"

# On veut pas que less affiche les lignes longues sur plusieurs lignes
export LESS="-S "

								
# On veut que less montre les caracteres non affichables par un # souligne
export LESSBINFMT='*u#'

# On veut que less affiche les trucs en français
#export LESSCHARSET=latin1
					
# On veut que les tris de fichier se fasse avec les repertoires d'abord
export LC_COLLATE=C
								
# On garde et on date tous les historiques meme quand il y 2 shells paralleles
shopt -s histappend
unset HISTFILESIZE
export HISTFILESIZE
unset HISTCONTROL
export HISTTIMEFORMAT="%s "
export PROMPT_COMMAND="history -a ; "'echo $$ ${USER} "$(history 1)" >> ~/.bash_eternal_history'

# On verifie la taille du terminal apres chaque commande			
shopt -s checkwinsize


# On veut l'auto-completion
test -r  /etc/bash_completion && source /etc/bash_completion

# End Of File
