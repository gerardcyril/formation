## Installation d'apache

yum install apache2
yum install httpd mariadb-server mariadb
systemctl enable httpd
systemctl start httpd

chown apache:apache -R /var/www

chmod 751 /var/www/cgi-bin
find /var/www/html -type f -exec chmod 644 {}\;
find /var/www/html -type d -exec chmod 755 {}\;

## Install PHP7

wget -q http://rpms.remirepo.net/enterprise/remi-release-7.rpm
wget -q https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -i remi-release-7.rpm epel-release-latest-7.noarch.rpm
yum-config-manager --enable remi-php72
yum install php
yum update

## Wordpress

wget https://fr.wordpress.org/wordpress-4.9.8-fr_FR.tar.gz
tar -xvf wordpress-4.9.8-fr_FR.tar.gz
mv wordpress /var/www/html/

mysql -u root -p
\. /wordpress.sql

*La suite se fait sur l'interface web*

## Mediawiki

wget https://releases.wikimedia.org/mediawiki/1.31/mediawiki-1.31.0.tar.gz
tar -xvf mediawiki-1.31.0.tar.gz
mv mediawiki-1.31.0 /var/www/html/mediawiki

yum install php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo php-xml php-mbstring

mysql -u root -p
\. /mediawiki.sql

*La suite se fait sur l'interface web*

## sauvegarde de base de données
mysqldump --databases wordpress my_wiki > mes-bases-$(date +%F).sql

cd /var/www
tar zcpvf webserver-$-$(hostname)-$(date +%F).tgz .

mysql -u root < mon_dump.sql

tar zxvf webserver.tgz
mv html/ /var/www/html

*Ne pas oublier le fichier de config*


