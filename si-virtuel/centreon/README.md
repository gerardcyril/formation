Installation 


~~~
wget http://yum.centreon.com/standard/3.4/el7/stable/centreon-stable.repo -O /etc/yum.repos.d/centreon-stable.repo
~~~

~~~
vim /etc/pki/rpm-gpg/RPM-GPG-KEY-CES

	-----BEGIN PGP PUBLIC KEY BLOCK-----
	Version: GnuPG v1.4.5 (GNU/Linux)

	mQGiBEy28fYRBADHbNta9G3rwLDRdN+v4KjjNVkFWr1AgbjHW8 t1dH3jE/RW99HL
	VJFoW1pS29BwZG/LjQiQbHJkn9cqzOxV26LMMAdncfeAiXEBVzQMH1lRBw/LmIQz
	QgLcvsldpn04Op0qNl3ZFFZnnKYZsIn16Du1uNqfClOBiceL2l Xvk7k0vwCggd5F
	DtRJJvFQycOq/1/M/8ZuXgMD/3jYXLDfPDVRQZMeGN25jcn4o+LhvNOqpeBe7rfF
	xSwBi1fwh0lvNqPTR7GXxvtfaYeU9swMZZ/+ZMbPgBJ29mnzpo3yfJrt3fP9WgOe
	mgOTBH+96YAHXY+jWxi+Lk1oxQCbdPMbtZ59wz6WCirb0X+89q LvouXPCZzMp/eG
	2a77A/9S9sKa6fbX74a24suurN+ZhmZZSEJldh3HoofByoF92Gzv2DKK ZIpFb1GN
	qFa8jVt4JVF60N3bn9ir6uxno5ARuSWTVIF+WTvoNMiEdjvWRU 3oLJtJL1luUyLT
	Dffb8NEej3e87IEH/4ehA6mv7a6i/7zrY5nDf5zXiqZ3HsGMSbRGQ2VudHJlb24g
	RW50ZXJwcmlzZSBTZXJ2ZXIgT2ZmaWNpYWwgU2lnbmluZyBLZX kgPGNlcy1rZXlA
	Y2VudHJlb24uY29tPohgBBMRAgAgBQJMtvH2AhsDBgsJCAcDAg QVAggDBBYCAwEC
	HgECF4AACgkQ9vxK44p2UrywwQCfSC57qk19N6sBRcbAi+wwbp gmLRQAnAjONYcT
	J4DasO87MOFV2oaj7dEpuQINBEy28fwQCAC2W2g5LoyOOOaGKm AvClee2dbLqMj/
	b+rdk06FN9avQCg7q45IL1E9j2b4iZayY/DSxB+FZ1bu2vDkci9pMPOdiv/aa+i6
	VyFQOO4/LgiNI9siMczf+Uwy5SqOUJNH/6c2Dy+PvYTOqx4F650EnQCVfy1lxMUS
	EiWIz21RfnXIJgC4TzJJHr8+hfnORZp44g5PwuA1qgdgRCgbe8 WA+mcKsacC639P
	fD4iCLYnruNfMKBxIn9lJCmAlSRRdtnvG8UvUtPcdC3USXWnGm p8IZsJk8fq3Pdb
	aQqLj5gifdpfyN9kch+jSGmwOnXBQjoJVahoO/vHYlFPCFS4sFHtQ+ZXAAMFB/94
	HBPCobYYfD1sG+LUBxBTOfhAWGF26dIR5GUfuI0e6xdqj6tzUz S9rMznQEWK93pE
	QX7fKCzOYnpime85L7YHHDi2Buk69qJnYiB4RJyY1hKloHCOVM bhXxsCBStEphZM
	j5irY0igeOPRj/NW/h75l3FWXmU51TdoiBSE5NRKqCkj+8thAKLbA99B3Oo5/jlG
	7BHQDyVlYAE1vMXKXjl17B9TnTJITkzERebqoQiVwngWRvgsHB l8QG8Jh43GQty5
	NqpkHshLED37V5rbp9EOIY1nugxUpZSUcSSHNvWUk34yL9XGZE 32L0js69MGT0hq
	/6pVMNcgBjtJoqo+p1GLiEkEGBECAAkFAky28fwCGwwACgkQ9vx K44p2UryUzwCe
	KFbF6AzjpXX4QuXAL2FwkRpaZfEAnAvM8pfjca6BVWlT3bmimK bMYkGP
	=+us4
	-----END PGP PUBLIC KEY BLOCK-----
~~~

~~~
yum install centreon-base-config-centreon-engine centreon centreon-poller-centreon-engine
~~~

~~~
yum -y install mariadb-server
systemctl enable mariadb
systemctl start mariadb
mysql_secure_installation
~~~

~~~
vim /etc/php.ini
	date.timezone Europe/Paris

vim /etc/systemd/system/mariadb.service.d/centreon.conf

	[Service]
	LimitNOFILE=32000
~~~

~~~
systemctl daemon-reload
systemctl restart mariadb
systemctl restart httpd
systemctl enable mariadb
systemctl enable httpd
~~~

Rendez-vous sur http://centreon.cyril.poe

