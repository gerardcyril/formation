Installation


~~~
yum install -y pacemaker pcs psmisc policycoreutils-python
~~~

Configuration

~~~
systemctl start pcsd
systemctl enable pcsd

passwd hacluster
~~~

~~~
pcs cluster auth www1.cyril.poe www2.cyril.poe
pcs cluster setup --name moncluster www1.cyril.poe www2.cyril.poe

pcs cluster start --all
~~~

~~~
corosync-cfgtool -s
~~~

~~~
pcs property set stonith-enabled=false
crm_verify -L -V

pcs resource create ClusterIP ocf:heartbeat:IPaddr2 ip=10.4.0.10 cidr_netmask=32 op monitor interval=30s
~~~

~~~
pcs status
pcs cluster stop www1.cyril.poe
pcs status
pcs status # sur www2.cyril.poe
pcs cluster start www1.cyril.poe
~~~

Adresse du VIP : 10.4.0.10

