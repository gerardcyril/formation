Installation

~~~
yum install -y httpd mariadb mariadb-server
yum install -y epel-release && yum -y update
~~~

~~~
yum install yum-utils
yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum-config-manager --enable remi-php72
yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo

php -v
~~~

Configuration

~~~
vi /etc/httpd/conf/httpd.conf
~~~

~~~
systemctl enable httpd
systemctl restart httpd
systemctl enable mariadb
systemctl start mariadb
~~~

~~~
cd /var/www/html
wget https://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz
tar -xvzf dokuwiki-stable.tgz
mv dokuwiki-20* dokuwiki
~~~

Rendez-vous sur http://wiki.yourdomain.com

~~~
mysql_secure_installation
~~~

~~~
cd /var/www/html/
wget https://fr.wordpress.org/wordpress-4.9.8-fr_FR.tar.gz
tar -xvf wordpress-4.9.8-fr_FR.tar.gz
rm wordpress-4.9.8-fr_FR.tar.gz
chown -R apache. wordpress
~~~

~~~
mysql -u root -p < wordpress.sql
~~~

Rendez-vous sur http://blog.yourdomain.com


