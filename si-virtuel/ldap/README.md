Installation

~~~
yum -y install openldap compat-openldap openldap-clients openldap-servers openldap-servers-sql openldap-devel
~~~

~~~
slappasswd # retenir le SSHA
~~~

Configuration

~~~
systemctl enable slapd
systemctl start slapd
~~~

~~~
vim db.ldif
ldapmodify -Y EXTERNAL  -H ldapi:/// -f db.ldif
~~~

~~~
vim monitor.ldif
ldapmodify -Y EXTERNAL  -H ldapi:/// -f monitor.ldif
~~~

~~~
vim base.ldif
ldapadd -x -W -D "cn=ldapadm,dc=cyril,dc=poe" -f base.ldif
~~~

~~~
vim user.ldif
ldapadd -x -W -D "cn=ldapadm,dc=cyril,dc=poe" -f user.ldif

ldappasswd -s mdp-user -W -D "cn=ldapadm,dc=cyril,dc=poe" -x "uid=user,ou=People,dc=cyril,dc=poe"
~~~

~~~
ldapsearch -x cn=user -b dc=cyril,dc=poe
~~~

Client

~~~
yum install -y openldap-clients nss-pam-ldapd authconfig.x86_64

authconfig --enableldap --enableldapauth --ldapserver=10.4.0.4 --ldapbasedn="dc=cyril,dc=poe" --enablemkhomedir --update
systemctl restart nslcd
~~~

~~~
getent passwd user
~~~

PhpLDAPadmin <a id="phpldap"></a>

~~~
yum install epel-release
yum install phpldapadmin

vi /etc/phpldapadmin/config.php
ligne 397 : décommenter
ligne 398 : commenter

vi /etc/httpd/conf.d/phpldapadmin.conf

Require all granted

systemctl restart httpd
~~~

Rendez-vous sur ldap.cyril.poe/ldapadmin
- User : cn=ldapadm,dc=cyril,dc=poe
- Password : password

