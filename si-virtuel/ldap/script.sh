#!/bin/bash 

# Extraction des donnees dans le fichier csv
extract (){
	uidnumber=`echo $1 | cut -d , -f 1`
	firstname=`echo $1 | cut -d , -f 2`
	lastname=`echo $1 | cut -d , -f 3`

	password=`echo $1 | cut -d , -f 5`
	departement=`echo $1 | cut -d , -f 6`
}


# verifie si il existe des doublons
verif (){
	for i in ${!tab[*]} ; do
		if [ "${tab[$i]}" =  "$1" ];
		then
			first=$(echo $firstname | cut -c1-2)
			uid=$(echo $first$lastname | tr '[:upper:]' '[:lower:]')
		fi
	done
}

# Declaration du tableau de doublons
tab=( toto )

# Parcours du fichier et generation du fichier ldif
for a in `tail +2 poe-users.csv` 
do
	extract $a
	first=$(echo $firstname | cut -c1)
	uid=$(echo $first$lastname | tr '[:upper:]' '[:lower:]')
	verif $uid

	echo dn: uid=$uid,ou=$departement,ou=People,DC=cyril,DC=poe
	echo objectclass: top
	echo objectclass: account
	echo objectclass: posixAccount
	echo objectclass: shadowAccount
	echo cn: $firstname $lastname
	echo uid: $uid
	echo uidnumber: $uidnumber
	echo gidnumber: $uidnumber
	echo homedirectory: /home/$uid
	echo loginshell: /bin/bash
	echo userPassword: $password
	echo
	tab[${#tab[*]}]=$uid
done > poe.user.ldif
