Installation 

~~~
yum install dhcp dhcp-utils
~~~

Configuration

~~~
vim /etc/dhcp/dhcpd.conf
~~~

Démarrage du service

~~~
systemctl enable dhcp
systemctl start dhcp
~~~

