Installation

~~~
yum install -y epel-release
yum install -y postfix
~~~

~~~
vim /etc/postfix/main.cf

	myhostname = mail.cyril.poe
	mydomain = cyril.poe
	myorigin = $mydomain
	inet_interfaces = all
	inet_protocols = ipv4
	mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain
	mynetworks = 127.0.0.0/8, 10.4.0.0/24
	home_mailbox = Maildir/
~~~

~~~
systemctl enable postfix
systemctl restart postfix
systemctl status postfix
~~~

~~~
yum install telnet

adduser user
passwd user
~~~

~~~
telnet localhost smtp

	Trying ::1...
	telnet: connect to address ::1: Connection refused
	Trying 127.0.0.1...
	Connected to localhost.
	Escape character is '^]'.
	220 mess.cyril.poe ESMTP Postfix
$	ehlo localhost
	250-mess.cyril.poe
	250-PIPELINING
	250-SIZE 10240000
	250-VRFY
	250-ETRN
	250-ENHANCEDSTATUSCODES
	250-8BITMIME
	250 DSN
$	mail from:<user>
	250 2.1.0 Ok
$	rcpt to:<user>
	250 2.1.5 Ok
$	data
	354 End data with <CR><LF>.<CR><LF>
$	Mon message de test
$	.
	250 2.0.0 Ok: queued as 00F8534FB
$	quit
	221 2.0.0 Bye
	Connection closed by foreign host.

~~~

~~~
cat /home/user/Maildir/new/1539267685.Vfd0cI60012M483963.messagerie
~~~

*Ajout des utilisateurs LDAP*

~~~
yum install -y openldap-clients nss-pam-ldapd authconfig.x86_64
authconfig --enableldap --enableldapauth --ldapserver=10.4.0.4 --ldapbasedn="dc=cyril,dc=poe" --enablemkhomedir --update
systemctl restart nslcd
~~~

## Dovecot

~~~
yum install dovecot
~~~


~~~
vim  /etc/dovecot/dovecot.conf
	24 : protocols = imap
	30 : listen = *

vim /etc/dovecot/conf.d/10-mail.conf
	30 : mail_location = maildir:~/Maildir

vim /etc/dovecot/conf.d/10-ssl.conf
	8: ssl = no
~~~

~~~
vim /etc/postfix/main.cf
#EOF
# Authentification SMTP
smtpd_sasl_type = dovecot
smtpd_sasl_path = private/auth
smtpd_sasl_auth_enable = yes
smtpd_sasl_security_options = noanonymous
smtpd_sasl_local_domain = $myhostname
smtpd_recipient_restrictions = permit_mynetworks,
                               permit_auth_destination,
                               permit_sasl_authenticated,
                               reject
~~~


~~~
systemctl enable dovecot
systemctl start dovecot
systemctl status dovecot
~~~

## Evolution

~~~
apt install evolution
~~~
