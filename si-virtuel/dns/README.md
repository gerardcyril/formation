Installation

~~~
yum install bind bind-utils
~~~

Configuration

~~~
vim /etc/named.conf

vim /var/named/cyril.poe.zone

vim /var/named/cyril.poe.reverse
~~~

Démarrage du service

~~~
systemctl enable named
systemctl start named
~~~
