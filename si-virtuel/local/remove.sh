#!/bin/bash

for machine in $(ls /mnt/backup); do
	find $machine/config/complete/*.tar.gz -mtime +30 -exec rm -vf {} \; | logger
done