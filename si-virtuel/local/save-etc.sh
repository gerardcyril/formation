#!/bin/bash

ajrd=$(date +"%y-%m-%d")

# Un peu de beauté
green=$(tput setaf 76)
red=$(tput setaf 1)
reset=$(tput sgr0)
e_success() { printf "${green}✔ %s${reset}\n" "$@" 
}
e_error() { printf "${red}✖ %s${reset}\n" "$@" 
}

# Création des variables
		savedata=$HOSTNAME-$ajrd
		# Attention le chemin doit appartenir à formation ou vous devrez connaitre le mot de passe root
		dest="formation@backup.cyril.poe:/mnt/backup/$HOSTNAME/config/complete"
	# Sauvegarde du dossier /etc/
		tar -cvzf $savedata.tar.gz "/etc/"
		scp $savedata.tar.gz $dest && e_success "Envoi Réussie" || e_error "Envoi Raté"
		rm -rf $savedata.tar.gz
