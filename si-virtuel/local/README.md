Configuration du réseau

~~~
vim /et/network/interfaces
~~~

Création et envoi des clefs SSH

~~~
ssh-keygen

ssh-copy-id -i .ssh/id_rsa_cyril.pub formation@10.4.0.2
~~~

Redirection de port

~~~
sudo ip addr add 192.168.20.146/24 dev vmbr0
sudo ip addr add 192.168.20.147/24 dev vmbr0
sudo ip addr add 192.168.20.148/24 dev vmbr0


iptables -t nat -A PREROUTING -p tcp -s 0/0 -d 192.168.20.146 --dport 80 -j DNAT --to 10.4.0.8:80
iptables -t nat -A PREROUTING -p tcp -s 0/0 -d 192.168.20.147 --dport 80 -j DNAT --to 10.4.0.8:9000
iptables -t nat -A PREROUTING -p tcp -s 0/0 -d 192.168.20.148 --dport 80 -j DNAT --to 10.4.0.10:80
~~~

