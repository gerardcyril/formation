Création de l'arborescence de *backup*

~~~
mkdir ldap messagerie pve stockage supervision web1 web2
for dir in */; do mkdir -- "$dir/"{config,data}; done
for dir in */; do mkdir -- "$dir/config/"{complete,differentielle}; done
for dir in */; do mkdir -- "$dir/data/"{complete,differentielle}; done
~~~

Mise du script remove.sh dans cron

~~~
vim /etc/crontab
	0 19 * * 4 /home/root/remove.sh
~~~

Ecoute pour les logs de nos client

~~~
vi /etc/rsyslog.conf
	# provides UDP syslog reception
	module(load="imudp")
	input(type="imudp" port="514")

~~~

Sur les machines toutes les machines **(pas fait)**

~~~
vim /etc/crontab
	0 17 * * 5 /root/save-etc.sh

vim /etc/rsyslog.conf
	*.*		@10.4.0.7:514
~~~

## Partage NFS

Sur backup.cyril.poe

~~~
yum install nfs-utils

vi /etc/exports
	# Partage du répertoire /mnt/html avec les hôtes 10.4.0.5 et 10.4.0.6 et les droits de lectures et écritures
	/mnt/html 10.4.0.0/24(rw,sync)

systemctl restart nfs
~~~

~~~
mkdir -p /home/formation/html
mount -t nfs 10.4.0.7:/mnt/html /home/formation/html
ll /home/formation/html
~~~

Sur l'hyperviseur

~~~
sed -i '$ i\  mount fstype=nfs,' /etc/apparmor.d/lxc/lxc-default-cgns
systemctl reload apparmor
~~~

Sur le container

~~~
yum install nfs-utils
sed -i '$a 10.4.0.7:/mnt/html  /var/www/html nfs rw 0 0' /etc/fstab
mount -a
df -h
~~~

Je bloque au montage 
- Sur une machine test : *mount: 10.4.0.7:/mnt/html is write-protected, mounting read-only*
- Sur www1 : *mount.nfs: access denied by server while mounting 10.4.0.7:/mnt/html*

