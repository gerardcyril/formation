## Portainer

Installation

~~~
curl -fsSL https://get.docker.com/ | sh

usermod -aG docker root
usermod -aG docker formation
~~~

Portainer

~~~
systemctl enable docker
systemctl start docker
systemctl status docker
~~~

~~~
docker run hello-world
docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer
~~~

Rendez-vous sur http://docker.cyril.poe:9000

user : admin
password : password


## AWX

Installation 

~~~
yum install ansible
~~~

~~~
ssh-keygen

ssh-copy-id -i .ssh/id_rsa.pub formation@10.4.0.2
~~~

~~~
vi /etc/ansible/hosts
~~~

~~~
ansible -m ping all
ansible -b -m yum -a "name=mlocate state=latest" lxcc -K
~~~

~~~
yum -y install epel-release
systemctl disable firewalld
systemctl stop firewalld
iptables -F
iptables -X
setenforce 0

sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
yum -y install git gettext ansible nodejs npm gcc-c++ bzip2
yum -y install python-docker-py
systemctl status docker
git clone https://github.com/ansible/awx.git

cd awx/installer/
ansible-playbook -i inventory install.yml
docker logs -f awx_task
~~~

Rendez-vous sur http://docker.cyril.poe

user = admin
password = password

