# Projet SI Virtuel

**Utilisateur de toutes mes VM**
- root:password
- formation:formation

### TODO List

- Acces web 443
- Playbook ansible
- RoundCube messagerie
- ELK

### J1

- [Installation de proxmox](local)
- [Mise en place d'un bridge](local/interfaces)
- [Création d'un template centos](template)
- [Serveur DHCP](dhcp)

## Jour 2

- [Serveur DNS](dns)
- [Serveur OpenLDAP](ldap)
- [Clef SSH](local)
- Tri du Git

## Jour 3

- [Serveur Web](web)
- [Cluster](cluster) **La VIP est en 10.4.0.10**
- [Serveur Stockage](backup)
- [Serveur Docker](docker)

## Jour 4

- [Script pour poe-users.csv](ldap/script.sh)
- [Redirection IP](local/README.md)
- [Installation de PhpLDAPadmin](ldap/README.md#phpldap)
- [Serveur messagerie](messagerie)

## Jour 5

- [Script sauvegarde /etc](local/save-etc.sh)
- [Script suppression +30 Jours](local/remove.sh)
- [Serveur de backup](backup)
	- Arborescence
	- Script 1 & 2
	- Ecoute log
	- NFS "web"
- [Centreon](centreon)

## Note

Environnement VM
- Centos7
- user: formation groupe: wheel
- package: vim, curl, sudo , openssh-server, mlocate
